package com.nerds.stocks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class StrategistRouterApplication {

	public static void main(String[] args) {
		SpringApplication.run(StrategistRouterApplication.class, args);
	}

}
